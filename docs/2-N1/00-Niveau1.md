# Correction du Niveau 1

Ce corrigé est là pour aider les élèves en difficulté, mais aussi pour ceux qui ont bien résolu les problèmes.

Pour chaque problème présenté ici :

- Bien relire l'énoncé,
- puis comparer la solution officielle et la variante proposée,
- lire les commentaires associés.

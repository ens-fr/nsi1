# Conditions avancées, opérateurs booléens

[Conditions avancées, opérateurs booléens](http://www.france-ioi.org/algo/chapter.php?idChapter=648)

## Espion étranger

### Sujet

On vous donne un intervalle de temps pendant lequel on sait qu'un espion est arrivé, puis la date d'arrivée d'un certain nombre de personnes. Déterminez combien de ces personnes peuvent être cet espion.

Votre programme doit d'abord lire deux entiers : la date de début et la date de fin de l'intervalle pendant lequel on sait que l'espion est arrivé en ville. Il doit ensuite lire un entier nbEntrées, le nombre total de personnes entrées dans la ville, puis les nbEntrées nombres suivants qui représentent les dates d'entrée (non triées) des différentes personnes.

Votre programme doit afficher le nombre de personnes entrées entre les deux dates données, incluses.

### Exemple

entrée :

    6
    10
    5
    7
    11
    8
    3
    6

sortie :

    3

### Solution officielle

```python
dateDébut = int(input())
dateFin = int(input())
nbEntrées = int(input())
nbPersonnes = 0
for loop in range(nbEntrées):
   date = int(input())
   if dateDébut <= date and date <= dateFin:
      nbPersonnes = nbPersonnes + 1
print(nbPersonnes)
```

### Solution alternative

```python
date_début = int(input())
date_fin = int(input())
nb_entrées = int(input())

nb_suspects = 0
for _ in range(nb_entrées):
    date = int(input())
    if date_début <= date <= date_fin:
        nb_suspects += 1

print(nb_suspects)
```

!!! tip "tester un intervalle"
    Avec Python, on peut tester facilement si un nombre est dans un intervalle.

    Au lieu de `#!python (a < x) and (x < b)` on peut écrire `#!python a < x < b`


## Maison de l'espion

### Sujet

On vous décrit une zone de recherche rectangulaire, parallèle aux axes, puis la position d'un certain nombre de maisons. Écrivez un programme qui détermine combien de maisons sont dans cette zone.

Votre programme devra lire, dans l'ordre : l'abscisse minimale, l'abscisse maximale, l'ordonnée minimale et l'ordonnée maximale du rectangle. Il lira ensuite le nombre total de maisons, puis pour chaque maison, son abscisse et son ordonnée.

Votre programme devra déterminer puis afficher le nombre de maisons qui se trouvent dans la zone de recherche. Si une maison est exactement sur le bord de la zone, elle doit être comptée.

### Exemple

entrée :

    1
    4
    1
    8
    12
    1
    7
    1
    9
    2
    3
    3
    2
    3
    4
    3
    6
    3
    9
    5
    3
    5
    8
    7
    5
    8
    2
    8
    8

sortie :

    5

### Solution officielle

```python
xMin = int(input())
xMax = int(input())
yMin = int(input())
yMax = int(input())
nbMaisons = int(input())
nbAFouiller = 0
for loop in range(nbMaisons):
   x = int(input())
   y = int(input())
   if (xMin <= x) and (x <= xMax) and (yMin <= y) and (y <= yMax):
      nbAFouiller = nbAFouiller + 1
print(nbAFouiller)
```

### Solution alternative

```python
x_min = int(input())
x_max = int(input())
y_min = int(input())
y_max = int(input())
nb_maisons = int(input())

for _ in range(nb_maisons):
    x = int(input())
    y = int(input())
    if (x_min <= x <= x_max) and (y_min <= y <=y_max):
        nb_maisons_suspectes += 1

print(nb_maisons_suspectes)
```

!!! tip "Augmenter de 1"
    Il est très fréquent que l'on augmente une variable de 1.

    On peut utiliser `#!python x += 1` à la place de `#!python x = x + 1`

    De manière générale :

    * `#!python x += a` signifie `#!python x = x + a`
    * `#!python x -= a` signifie `#!python x = x - a`
    * `#!python x *= a` signifie `#!python x = x * a`
    * `#!python x //= a` signifie `#!python x = x // a`
    * `#!python x %= a` signifie `#!python x = x % a`
    * et il y en d'autres...

## Nombre de jours dans le mois

Écrivez un programme qui lit un numéro de mois algoréen, et affiche le nombre de jours de celui-ci. Les Algoréens disposent de leur propre calendrier. Voici les informations dont vous avez besoin :

|Numéro du mois | Nombre de jours|
|:---:|:---:|
|1|30|
|2|30|
|3|30|
|4|31|
|5|31|
|6|31|
|7|30|
|8|30|
|9|30|
|1|31|
|1|29|

### Exemple

entrée :

    6

sortie :

    31

### Solution officielle

```python
numero = int(input())
if numero == 11:
   print(29)
else:
   if ( (4 <= numero) and (numero <= 6) ) or (numero == 10):
      print(31)
   else:
      print(30)
```

### Solution alternative

On peut utiliser, au lieu de conditions, une table de valeurs.

```python
# le mois zéro n'existe pas, on le code avec -1 jours !
nb_jours = [-1, 30, 30, 30, 31, 31, 31, 30, 30, 30, 31, 29]

numéro = int(input())
print(nb_jours[numéro])
```

!!! warning "Attention aux indices"
    Le $-1$ de la liste, correspond au mois d'indice $0$ qui n'existe pas. On peut mettre ce qu'on veut à la place de $-1$, comme $0$ ou `None`. Cependant, pour avoir un tableau strictement homogène en type de donnée, on pourrait recommander de ne pas mettre `None`. Avec Python, dans certains cas, on accepte de mélanger `None` avec d'autres types de donnés.

## Amitié entre gardes

### Sujet

Vous devez écrire un programme qui détermine si deux soldats ont été de garde en même temps.

Votre programme doit lire quatre entiers : la date du début et la date de fin (incluse) du service du premier soldat puis celles du second soldat.

Si les deux soldats ont, à un moment (même une seule seconde), été de garde en même temps le programme devra écrire "Amis" et sinon "Pas amis".

### Exemples

#### Exemple 1

entrée :

    2
    5
    3
    6

sortie :

    Amis

#### Exemple 2

entrée :

    1
    5
    10
    15

sortie :

    Pas amis

#### Exemple 3

entrée :

    2
    4
    4
    6

sortie :

    Amis

### Solution officielle

```python
dateDebutPremier = int(input())
dateFinPremier = int(input())
dateDebutSecond = int(input())
dateFinSecond = int(input())
if (dateFinSecond < dateDebutPremier) or (dateFinPremier < dateDebutSecond):
   print("Pas amis")
else:
   print("Amis")
```

!!! info "Choisir le plus simple"

    - Il est plus simple de justifier qu'on est `Pas amis` :

        - soit le premier est déjà parti quand le second arrive,
        - soit l'inverse !
    
    - Pour `Amis`, il y aurait 4 cas à envisager...

### Solution alternative

- En utilisant la loi de De Morgan, on peut donner une condition aussi rapide qui valide "Amis" au lieu de "Pas amis".

$$\text{non}(A \text{ ou } B) = \text{non}(A)\text{ et }\text{non}(B)$$

```python
début_1 = int(input())
fin_1 = int(input())
début_2 = int(input())
fin_2 = int(input())

if (fin_2 >= début_1) and (fin_1 >= début_2):
    print("Amis")
else:
    print("Pas amis")
```


## L'espion est démasqué !

### Sujet

Votre programme doit lire entier : un nombre de personnes à considérer. Ensuite, pour chaque personne, il doit lire son signalement sous la forme de cinq entiers : sa taille en centimètres, son âge en années, son poids en kilogrammes, un entier valant 1 si la personne possède un cheval et 0 sinon, et un entier valant 1 si la personne a les cheveux bruns et 0 sinon.

On veut déterminer pour chaque personne à quel point elle correspond aux 5 critères suivants :

- il aurait une taille supérieure ou égale à 178 cm et inférieure ou égale à 182 cm ;
- il aurait au moins 34 ans ;
- il pèserait strictement moins de 70 kg ;
- il n'a pas de cheval ;
- il a les cheveux bruns.

Lorsque cela n'est pas précisé explicitement, les inégalités sont au sens large.

Pour chaque personne, vous devez tester tous les critères. S'ils sont vérifiés tous les 5, vous devez afficher « Très probable ». Si seulement 3 ou 4 sont vérifiés, vous devez afficher « Probable ». Si aucun n'est vérifié, vous devez afficher « Impossible », et dans les autres cas, vous devez afficher « Peu probable ».

Exemple

entrée :

    1
    180
    40
    65
    0
    1

sortie :

    Très probable

### Solution officielle

```python
nbPersonnes = int(input())
for loop in range(nbPersonnes):
   nbCriteres = 0
   taille = int(input())
   if (178 <= taille) and (taille <= 182):
      nbCriteres = nbCriteres + 1
   age = int(input())
   if age >= 34:
      nbCriteres = nbCriteres + 1
   poids = int(input())
   if poids < 70:
      nbCriteres = nbCriteres + 1
   aCheval = int(input())
   if aCheval == 0:
      nbCriteres = nbCriteres + 1
   aLesCheveuxBruns = int(input())
   if aLesCheveuxBruns == 1:
      nbCriteres = nbCriteres + 1
   
   if nbCriteres == 0:
      print("Impossible")
   elif nbCriteres == 5:
      print("Très probable")
   elif nbCriteres >= 3:
      print("Probable")
   else:
      print("Peu probable")
```

### Solution alternative

- On utilise une table de résultats (`conclusion`) ; utile pour une fonction ayant peu d'antécédents.

```python
conclusion = ["Impossible", "Peu probable", "Peu probable",\
              "Probable", "Probable", "Très probable"]

nb_personnes = int(input())
for _ in range(nb_personnes):
    taille = int(input())
    âge = int(input())
    poids = int(input())
    avec_cheval = int(input())
    est_brun = int(input())
    
    nb_critères = 0
    if 178 <= taille <= 182:
        nb_critères += 1
    if âge >= 34:
        nb_critères += 1
    if poids < 70:
        nb_critères += 1
    if avec_cheval == 0:
        nb_critères += 1
    if est_brun == 1:
        nb_critères += 1
    
    print(conclusion[nb_critères])
```


# Cours de NSI première année

Auteur : [Franck CHAMBON](https://lyc-84-bollene.gitlab.io/chambon/)

## Leçons

- [Le cours sur Python](https://ens-fr.gitlab.io/python_maths/).

- Dans les pages suivantes, les autres chapitres du cours.

## Exercices

1. [France-IOI](http://www.france-ioi.org/)
    - Il faut y travailler 2 à 3 fois par semaine.
    - Penser à refaire certains exercices précédents, plus rapidement.

## Côté ludique

1. Un jeu : [Py-rates](https://py-rates.fr/index.html)
    - C'est du Python. Facile.

2. Un jeu : [CargoBot](http://www-verimag.imag.fr/~wack/CargoBot/)
    - Vous devez programmer un automate. Beaucoup de niveaux faciles, mais aussi des difficiles.

3. Un jeu : [RoboZZle](http://www.robozzle.com/beta/index.html?puzzle=-1)
    - Vous devez récupérer toutes les étoiles. Quelques niveaux faciles, beaucoup de niveaux qui feront réfléchir.

4. Un jeu : [Cube Composer](https://david-peter.de/cube-composer/)
    - Appliquer des fonctions pour transformer un objet en forme de cubes colorés en un autre. Quelques niveaux faciles, assez vite difficile.
